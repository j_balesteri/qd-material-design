'use strict';

app.controller('MainCtrl', function ($scope, $mdDialog, $mdToast) {
    $scope.message = 'Welcome to qumani designs';
    $scope.featuredProjects = [
      {
        title: 'Project Title 1',
        image: 'http://placehold.it/320x180/aaa.png/888&amp;text=PROJECT+IMAGE',
        desc: 'Lorem ipsum dolor sit amet, audiam corpora sed ad, mea ea illud postea. At pri duis minimum, equidem pertinax consequuntur.'
      },
      {
        title: 'Project Title 2',
        image: 'http://placehold.it/320x180/aaa.png/888&amp;text=PROJECT+IMAGE',
        desc: 'Lorem ipsum dolor sit amet, audiam corpora sed ad, mea ea illud postea. At pri duis minimum, equidem pertinax consequuntur.'
      },
      {
        title: 'Project Title 3',
        image: 'http://placehold.it/320x180/aaa.png/888&amp;text=PROJECT+IMAGE',
        desc: 'Lorem ipsum dolor sit amet, audiam corpora sed ad, mea ea illud postea. At pri duis minimum, equidem pertinax consequuntur.'
      },
      {
        title: 'Project Title 4',
        image: 'http://placehold.it/320x180/aaa.png/888&amp;text=PROJECT+IMAGE',
        desc: 'Lorem ipsum dolor sit amet, audiam corpora sed ad, mea ea illud postea. At pri duis minimum, equidem pertinax consequuntur.'
      }
    ]

    var selectDialog = $scope.featuredProjects[0].title;
    var dialogContent = ' \
        <div layout-align="center center" layout-padding> \
            <h3>'+ selectDialog +'</h3> \
            <img flex="90" src="http://placehold.it/640x360/aaa.png/888&amp;text=PROJECT+IMAGE" alt"alt text"> \
            <p flex="90">Lorem ipsum dolor sit amet, audiam corpora sed ad, mea ea illud postea. At pri duis minimum, equidem pertinax consequuntur.</p> \
        </div> \
    ';
    var featuredProject ='<md-dialog flex="80" layout-align="center center">' + dialogContent + '</md-dialog>';

    function makeDialog(tmpl) {
        return function(ev) {
            $mdDialog.show({
                template: tmpl,
                targetEvent: ev,
                controller: 'DialogController'
            })
            .then(function(answer) {
                $mdToast.show(
                    $mdToast.simple()
                    .content(JSON.stringify(answer))
                );
            }, function() {
                $mdToast.show(
                    $mdToast.simple()
                    .content('cancelled')
                );
            });
        };
    }
    $scope.dialogNoForm = makeDialog(featuredProject);
});

app.controller('DialogController', function($scope, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

});

