'use strict';

app.controller('AboutCtrl', function ($scope, $mdDialog, $mdToast, $mdSidenav, $log) {
    $scope.message = 'About Qumani Designs';
});

app.controller('AppCtrl', function ($scope, $timeout, $mdSidenav, $log) {
  $scope.toggleRight = function() {
    $mdSidenav('right').toggle()
  };
})
.controller('RightCtrl', function($scope, $timeout, $mdSidenav, $log) {
  $scope.close = function() {
    $mdSidenav('right').close()
  };
});
